package ru.sherb.util;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StringUtilsTest {

    @Test
    void capitalize() {
        String expected = "Test";

        String actual = StringUtils.capitalize("test");

        assertEquals(expected, actual);
    }
}