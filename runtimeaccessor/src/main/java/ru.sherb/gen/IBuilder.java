package ru.sherb.gen;

public interface IBuilder<T> {
    
    IBuilder EMPTY_GENERATOR = new IBuilder() {
        @Override
        public IBuilder set(String fieldName, Object value) {
            return this;
        }

        @Override
        public Object build() {
            return new Object();
        }
    };

    IBuilder<T> set(String fieldName, Object value);

    T build();
}
