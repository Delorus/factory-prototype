package ru.sherb.access;

import ru.sherb.gen.IBuilder;
import ru.sherb.util.StringUtils;

import java.util.Map;
import java.util.WeakHashMap;

public final class BuilderFacade {

    private static final Map<String, IBuilder> cache = new WeakHashMap<String, IBuilder>();

    @SuppressWarnings("unchecked")
    public static <T> IBuilder<T> getBuilder(String rootName) {
        try {
            String className = "ru.sherb.gen.Builder$" + StringUtils.capitalize(rootName);
            if (cache.containsKey(className)) {
                return cache.get(className);
            }

            IBuilder<T> result = (IBuilder<T>) Class.forName(className).newInstance();
            cache.put(className, result);
            return result;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            // just ignore...
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            // just ignore...
        } catch (InstantiationException e) {
            e.printStackTrace();
            // just ignore...
        }
        return IBuilder.EMPTY_GENERATOR;
    }
}
