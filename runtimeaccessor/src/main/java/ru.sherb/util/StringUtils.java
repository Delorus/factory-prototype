package ru.sherb.util;

public final class StringUtils {

    private StringUtils() throws IllegalAccessException {
        throw new IllegalAccessException();
    }

    public static String capitalize(String s) {
        if (s == null || s.isEmpty()) {
            return s;
        }

        return String.valueOf(s.charAt(0)).toUpperCase() + s.substring(1);
    }
}
