package ru.sherb.gen;

import org.junit.jupiter.api.Test;
import ru.sherb.access.BuilderFacade;

import static org.junit.jupiter.api.Assertions.assertEquals;

class BuilderFacadeTest {

    @Test
    void deserialize() {
        IBuilder<PersonDto> builder = BuilderFacade.getBuilder("person");
        PersonDto expected = new PersonDto("ivan", true, 42);
        String xml = "<person><fio>ivan</fio><sex>true</sex><age>42</age></person>";

        PersonDto actual = builder
                .set("fio", "ivan")
                .set("sex", true)
                .set("age", 42)
                .build();

        assertEquals(expected, actual);
    }
}