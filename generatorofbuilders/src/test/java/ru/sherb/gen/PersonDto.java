package ru.sherb.gen;

import ru.sherb.proc.XmlRoot;
import ru.sherb.proc.XmlTag;

import java.util.Objects;

@XmlRoot("person")
public class PersonDto {

    @XmlTag("fio")
    private String name;

    @XmlTag
    private boolean sex;

    @XmlTag
    private int age;

    private int password;

    public PersonDto() {
    }

    public PersonDto(String name, boolean sex, int age) {
        this.name = name;
        this.sex = sex;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isSex() {
        return sex;
    }

    public void setSex(boolean sex) {
        this.sex = sex;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getPassword() {
        return password;
    }

    public void setPassword(int password) {
        this.password = password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final PersonDto personDto = (PersonDto) o;
        return sex == personDto.sex &&
                age == personDto.age &&
                Objects.equals(name, personDto.name);
    }

    @Override
    public int hashCode() {

        return Objects.hash(name, sex, age);
    }
}