package ru.sherb.proc;

import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.CodeBlock;
import com.squareup.javapoet.FieldSpec;
import com.squareup.javapoet.JavaFile;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.ParameterizedTypeName;
import com.squareup.javapoet.TypeName;
import com.squareup.javapoet.TypeSpec;
import ru.sherb.gen.IBuilder;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.Messager;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;
import javax.tools.Diagnostic;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@SupportedSourceVersion(SourceVersion.RELEASE_8) // TODO: 06.03.2018 change to RELEASE_6
@SupportedAnnotationTypes({"ru.sherb.proc.XmlRoot", "ru.sherb.proc.XmlTag"})
public class BuilderGenerateProcessor extends AbstractProcessor {

    // TODO: 06.03.2018 refactor
    private static class Pair<T, V> {
        public final T value1;
        public final V value2;

        public static <T, V> Pair<T, V> of(T value1, V value2) {
            return new Pair<>(value1, value2);
        }

        private Pair(T value1, V value2) {
            this.value1 = value1;
            this.value2 = value2;
        }
    }

    private ProcessingEnvironment procEnv;
    private Messager msg;

    @Override
    public synchronized void init(ProcessingEnvironment processingEnv) {
        super.init(processingEnv);
        procEnv = processingEnv;
        msg = processingEnv.getMessager();
    }

    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        msg.printMessage(Diagnostic.Kind.NOTE, "start " + this.getClass().getName());
        if (annotations == null || annotations.isEmpty()) {
            return false;
        }

        for (final Element element : roundEnv.getElementsAnnotatedWith(XmlRoot.class)) {

            ClassName className = ClassName.get((TypeElement) element);

            Map<String, Pair<TypeName, String>> setters = new HashMap<>();
            for (final Element enclosedElement : element.getEnclosedElements()) {
                if (enclosedElement.getKind() == ElementKind.FIELD && enclosedElement.getAnnotation(XmlTag.class) != null) {
                    String tagName = enclosedElement.getAnnotation(XmlTag.class).value();
                    if (tagName.isEmpty()) {
                        tagName = enclosedElement.getSimpleName().toString();
                    }
                    setters.put(tagName, Pair.of(
                                    ClassName.get(enclosedElement.asType()),
                                    "set" + capitalize(enclosedElement.getSimpleName().toString())));
                }
            }

            ParameterizedTypeName genericAnnotation = ParameterizedTypeName.get(
                    ClassName.get(IBuilder.class),
                    TypeName.get(element.asType()));

            FieldSpec dtoField = FieldSpec.builder(className, "dto", Modifier.PRIVATE)
                    .initializer("new $T()", className)
                    .build();

            MethodSpec setMethod = buildSetMethod(setters, genericAnnotation, dtoField);

            MethodSpec buildMethod = MethodSpec.methodBuilder("build")
                    .addAnnotation(Override.class)
                    .addModifiers(Modifier.PUBLIC)
                    .returns(className)
                    .addStatement("return $L", dtoField.name)
                    .build();

            String rootName = element.getAnnotation(XmlRoot.class).value();
            String builderName = "Builder$"
                    + String.valueOf(rootName.charAt(0)).toUpperCase()
                    + rootName.substring(1);

            TypeSpec builder = TypeSpec.classBuilder(builderName)
                    .addModifiers(Modifier.FINAL, Modifier.PUBLIC)
                    .addSuperinterface(genericAnnotation)
                    .addField(dtoField)
                    .addMethod(setMethod)
                    .addMethod(buildMethod)
                    .build();

            JavaFile javaFile = JavaFile.builder("ru.sherb.gen", builder)
                    .addFileComment("Automatically generated file, do not modify")
                    .build();

            try {
                javaFile.writeTo(procEnv.getFiler());
                msg.printMessage(Diagnostic.Kind.NOTE, "complete!", element);
            } catch (IOException e) {
                msg.printMessage(Diagnostic.Kind.ERROR, e.getMessage(), element);
                e.printStackTrace();
            }
        }

        return false;
    }

    private MethodSpec buildSetMethod(Map<String, Pair<TypeName, String>> setters, ParameterizedTypeName genericAnnotation, FieldSpec dto) {

        CodeBlock.Builder code = CodeBlock.builder();
        setters.forEach((tag, setter) -> {
            code.beginControlFlow("if($S.equals(fieldName))", tag)
                    .addStatement("$N.$L(($T) value)", dto, setter.value2, setter.value1.box())
                    .endControlFlow();
        });

        return MethodSpec.methodBuilder("set") //todo change to get method name from IBuilder.class
                .addAnnotation(Override.class)
                .addModifiers(Modifier.PUBLIC)
                .returns(genericAnnotation)
                .addParameter(String.class, "fieldName")
                .addParameter(Object.class, "value")
                .addCode(code.build())
                .addStatement("return this")
                .build();
    }

    // cope-paste from ru.sherb.util.StringUtils, because this class run in compile-time
    private static String capitalize(String s) {
        if (s == null || s.isEmpty()) {
            return s;
        }

        return String.valueOf(s.charAt(0)).toUpperCase() + s.substring(1);
    }
}
